require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const List = require('./pages/ListBook.vue').default;
const Create = require('./pages/CreateBook.vue').default;
const Detail = require('./pages/DetailBook.vue').default;
const NotFound = require('./pages/NotFound.vue').default;

const routes = [
    {
        path: '/create',
        component: Create
    },
    {
        path: '/list',
        component: List
    },
    {
        path: '/detail/:id?',
        component: Detail,
        props: true
    },
    {
        path: '/',
        component: List
    },
    {
        path: '*',
        component: NotFound
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

const app = new Vue({
    el: '#app',
    router
});
