<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterBook;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MasterBook::get();
        return response([
            'success' => true,
            'message' => 'Daftar Buku',
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createValidator = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'status' => 'required',
        ],
        [
            'title.required' => 'Masukkan Judu Buku',
            'author.required' => 'Masukkan Pengarang Buku',
            'publisher.required' => 'Masukkan Penerbit Buku',
            'status.required' => 'Masukkan Status Buku',
        ]);

        if($createValidator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Silahkan isi form yang kosong',
                'data' => $createValidator->errors()
            ], 400);
        }
        else {
            $createData = MasterBook::create([
                'title' => $request->input('title'),
                'author' => $request->input('author'),
                'publisher' => $request->input('publisher'),
                'status' => $request->input('status')
            ]);

            if($createData) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data buku berhasil disimpan'
                ], 201);
            }
            else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data buku gagal disimpan'
                ], 400);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MasterBook::whereId($id)->first();

        if($data) {
            return response()->json([
                'success' => true,
                'message' => 'Detail buku',
                'data'    => $data
            ], 200);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Buku tidak ditemukan',
                'data'    =>''
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateValidator = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'status' => 'required',
        ],
        [
            'title.required' => 'Masukkan Judu Buku',
            'author.required' => 'Masukkan Pengarang Buku',
            'publisher.required' => 'Masukkan Penerbit Buku',
            'status.required' => 'Masukkan Status Buku',
        ]);

        if($updateValidator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Silahkan isi form yang kosong',
                'data' => $updateValidator->errors()
            ], 400);
        }
        else {
            $updateData = MasterBook::whereId($id)->update([
                'title' => $request->input('title'),
                'author' => $request->input('author'),
                'publisher' => $request->input('publisher'),
                'status' => $request->input('status')
            ]);

            if($updateData) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data buku berhasil diperbaharui'
                ], 200);
            }
            else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data buku gagal diperbaharui'
                ], 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = MasterBook::findOrFail($id);
        $data->delete();

        if($data) {
            return response()->json([
                'success' => true,
                'message' => 'Data buku berhasil dihapus'
            ], 200);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Data buku gagal dihapus'
            ], 500);
        }
    }
}
