<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterBook extends Model
{
    protected $table = 'master_books';
    protected $fillable = ['title', 'author', 'publisher', 'status'];
}
