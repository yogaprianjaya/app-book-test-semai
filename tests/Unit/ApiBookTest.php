<?php

namespace Tests\Unit;

use App\MasterBook;
use Tests\TestCase;

class ApiBookTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $value = [
            'title' => 'New Book Bussines',
            'author' => 'Alexander',
            'publisher' => 'Great Book',
            'status' => 'Tersedia'
        ];
        $valueUpdate = [
            'title' => 'Update Book Bussines',
            'author' => 'Alexander Update',
            'publisher' => 'Great Book Update',
            'status' => 'Dipinjam'
        ];

        $this->get(url('/api/book'))->assertStatus(200);
        $this->post(url('/api/book'), $value)->assertStatus(201);

        $idData = MasterBook::latest()->first();

        $this->get(url('/api/book/'.$idData['id']))->assertStatus(200);
        $this->put(url('/api/book/'.$idData['id']), $valueUpdate)->assertStatus(200);
        $this->delete(url('/api/book/'.$idData['id']))->assertStatus(200);
    }
}
