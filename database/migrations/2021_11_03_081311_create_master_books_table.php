<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_books', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50);
            $table->string('author', 20);
            $table->string('publisher', 20);
            $table->enum('status', ['Tersedia', 'Dipinjam']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_books');
    }
}
